From 23162357097c5f295ee9143311b1f8dc058eb06d Mon Sep 17 00:00:00 2001
From: Adrien Destugues <pulkomandy@gmail.com>
Date: Sun, 18 Jan 2015 15:35:30 +0100
Subject: Adjust setvars.sh to Haiku build environment.


diff --git a/setvars.sh b/setvars.sh
index a64d135..30bce44 100755
--- a/setvars.sh
+++ b/setvars.sh
@@ -6,14 +6,14 @@
 #       modify it as necessary for your own use!!
 
 # Change this to point your Open Watcom source tree
-export OWROOT=/home/ow/ow
+export OWROOT=/sources/openwatcom-2.0.0
 
 # Subdirectory to be used for building OW build tools
 export OWOBJDIR=binbuild
 
 # Set this entry to identify your toolchain used by build process
 # supported values are WATCOM GCC CLANG
-export OWTOOLS=WATCOM
+export OWTOOLS=GCC
 
 # Documentation related variables
 
-- 
1.8.3.4


From 8eb0dea26f4e2d7e0d7924fa980660da52dc77f3 Mon Sep 17 00:00:00 2001
From: Adrien Destugues <pulkomandy@gmail.com>
Date: Mon, 19 Jan 2015 22:44:26 +0100
Subject: Fix local.mif for Haiku.

* It was still referencing BSD from which it was copied, resulting in
broken build.

diff --git a/build/mif/local.mif b/build/mif/local.mif
index 7a21e56..3f3b863 100644
--- a/build/mif/local.mif
+++ b/build/mif/local.mif
@@ -296,7 +296,7 @@ bld_cl_sys =
 
 bld_incs = $(bld_extra_incs) -I"$(watcom_dir)/h"
 
-bld_cppflags = $(common_cppflags_bsd) $(common_cppflags_$(bld_cpu))
+bld_cppflags = $(common_cppflags_haiku) $(common_cppflags_$(bld_cpu))
 
 bld_cflags   = $(common_cflags) $(common_flags) -o $@
 
@@ -328,8 +328,8 @@ cl  = clang -pipe
 
 librarian = ar
 
-cppflags_bsd   = $(common_cppflags_bsd)
-cppflags_osi   = $(common_cppflags_bsd)
+cppflags_haiku   = $(common_cppflags_haiku)
+cppflags_osi   = $(common_cppflags_haiku)
 cppflags_386   = $(common_cppflags_386)
 cppflags_ppc   = $(common_cppflags_ppc)
 cppflags_x64   = $(common_cppflags_x64)
-- 
1.8.3.4


From fb3e476980b1832ac3faff2a0c1f8501bff68890 Mon Sep 17 00:00:00 2001
From: Adrien Destugues <pulkomandy@gmail.com>
Date: Tue, 20 Jan 2015 12:36:48 +0100
Subject: trmem: fix build with non-watcom compiler

* i86.h is not available in this case, use the stubs.

diff --git a/bld/trmem/trmem.c b/bld/trmem/trmem.c
index f2c6eb9..a7ea28f 100644
--- a/bld/trmem/trmem.c
+++ b/bld/trmem/trmem.c
@@ -33,7 +33,7 @@
 #include <string.h>
 #include <ctype.h>
 
-#if defined( _M_IX86 )
+#if defined( _M_IX86 ) && defined( __WATCOMC__ )
 #include <i86.h>
 #endif
 
@@ -782,7 +782,7 @@ memsize _trmem_get_peak_usage( _trmem_hdl hdl ) {
     return( hdl->max_mem );
 }
 
-#ifndef _M_IX86
+#if !defined( _M_IX86 ) || !defined( __WATCOMC__)
 _trmem_who  _trmem_guess_who( void )
 /**********************************/
 /* NYI: stubbed for now */
-- 
1.8.3.4


From d08d3eac330bd61aaff31170eaa67de964874cd5 Mon Sep 17 00:00:00 2001
From: Adrien Destugues <pulkomandy@gmail.com>
Date: Tue, 20 Jan 2015 12:37:36 +0100
Subject: clibext: better implementation of _cmdname for haiku

We need to look for the app image and not take whatever is listed first.

diff --git a/bld/watcom/c/clibext.c b/bld/watcom/c/clibext.c
index d7fc1e0..f857228 100644
--- a/bld/watcom/c/clibext.c
+++ b/bld/watcom/c/clibext.c
@@ -1270,9 +1270,13 @@ char *_cmdname( char *name )
 
 char *_cmdname( char *name )
 {
-       image_info info;
-       get_image_info( 0, &info );
-       return( strcpy( name, info.name ) );
+    image_info info;
+    int32 cookie;
+    while ( get_next_image_info( 0, &cookie, &info ) == B_OK) {
+        if ( info.type != B_APP_IMAGE )
+            continue;
+        return( strcpy( name, info.name ) );
+    }
 }
 
 #elif defined( __UNIX__ )
-- 
1.8.3.4


From 2b23463882316209eecbaa20416eee963710ae87 Mon Sep 17 00:00:00 2001
From: Adrien Destugues <pulkomandy@gmail.com>
Date: Tue, 20 Jan 2015 12:38:16 +0100
Subject: Fix more WResFileOffset type mismatches

* Needed for things to build on Haiku.

diff --git a/bld/as/c/asmsg.c b/bld/as/c/asmsg.c
index 00f29c3..f7df3d5 100644
--- a/bld/as/c/asmsg.c
+++ b/bld/as/c/asmsg.c
@@ -70,7 +70,7 @@ static unsigned         msgShift;
 #define NO_RES_MESSAGE  "Error: could not open message resource file\r\n"
 #define NO_RES_SIZE     (sizeof(NO_RES_MESSAGE)-1)
 
-extern long             FileShift;
+extern WResFileOffset             FileShift;
 
 static HANDLE_INFO      hInstance = {0};
 
diff --git a/bld/wasm/c/wasmmsg.c b/bld/wasm/c/wasmmsg.c
index 51a406f..302bb9d 100644
--- a/bld/wasm/c/wasmmsg.c
+++ b/bld/wasm/c/wasmmsg.c
@@ -60,7 +60,7 @@ static const char *txtmsgs[] = {
 #define NO_RES_MESSAGE  "Error: could not open message resource file.\r\n"
 #define NO_RES_SIZE     (sizeof( NO_RES_MESSAGE ) - 1)
 
-extern  long            FileShift;
+extern  WResFileOffset            FileShift;
 
 static  HANDLE_INFO     hInstance = { 0 };
 static  unsigned        MsgShift;
diff --git a/bld/wl/c/loadpe.c b/bld/wl/c/loadpe.c
index 41b7457..e0e674f 100644
--- a/bld/wl/c/loadpe.c
+++ b/bld/wl/c/loadpe.c
@@ -773,7 +773,7 @@ WResFileSSize  RcWrite( WResFileID hdl, const void *buf, WResFileSize len )
     return( len );
 }
 
-long RcSeek( int hdl, long off, int pos )
+WResFileOffset RcSeek( WResFileID hdl, WResFileOffset off, int pos )
 {
     DbgAssert( pos != SEEK_END );
     DbgAssert( !(pos == SEEK_CUR && off < 0) );
@@ -794,7 +794,7 @@ long RcSeek( int hdl, long off, int pos )
     }
 }
 
-long RcTell( int hdl )
+WResFileOffset RcTell( WResFileID hdl )
 {
     DbgAssert( hdl == Root->outfile->handle );
 
diff --git a/bld/wres/c/loadfind.c b/bld/wres/c/loadfind.c
index 1449fa6..e8aa881 100644
--- a/bld/wres/c/loadfind.c
+++ b/bld/wres/c/loadfind.c
@@ -55,7 +55,7 @@ typedef struct dbgheader {
 } dbgheader;
 #include "poppck.h"
 
-long    FileShift = 0;
+WResFileOffset    FileShift = 0;
 
 unsigned char FindResources( PHANDLE_INFO hInstance )
 /* look for the resource information in a debugger record at the end of file */
-- 
1.8.3.4


From 85d972cda11f12d57fa958551805f02113193855 Mon Sep 17 00:00:00 2001
From: Adrien Destugues <pulkomandy@gmail.com>
Date: Tue, 20 Jan 2015 14:25:55 +0100
Subject: Another missing haiku define.


diff --git a/bld/watcom/h/wstd.h b/bld/watcom/h/wstd.h
index 01e8745..f892d58 100644
--- a/bld/watcom/h/wstd.h
+++ b/bld/watcom/h/wstd.h
@@ -82,6 +82,9 @@ typedef unsigned_8      sbit;
 #elif defined( __LINUX__ )
     #undef  OS_LINUX
     #define OS_LINUX            1
+#elif defined( __HAIKU__ )
+    #undef  OS_HAIKU
+    #define OS_HAIKU            1
 #elif defined( __BSD__ )
     #undef OS_BSD
     #define OS_BSD              1           
-- 
1.8.3.4


From 62d47ba56c04a1b6dddd343a96ddbb59bc83a97f Mon Sep 17 00:00:00 2001
From: Adrien Destugues <pulkomandy@gmail.com>
Date: Tue, 20 Jan 2015 14:26:59 +0100
Subject: Some missing local > static conversions.

* Not doing this really confuses gcc on Haiku.

diff --git a/bld/cc/c/ccmain.c b/bld/cc/c/ccmain.c
index 547fc50..a75b4db 100644
--- a/bld/cc/c/ccmain.c
+++ b/bld/cc/c/ccmain.c
@@ -81,7 +81,7 @@ static  bool        TryOpen( const char *path, const char *filename );
 static  void        ParseInit( void );
 static  void        CPP_Parse( void );
 static  bool        FCB_Alloc( FILE *fp, const char *filename );
-local   void        Parse( void );
+static  void        Parse( void );
 static  bool        OpenPgmFile( void );
 static  void        DelDepFile( void );
 static  const char  *IncludeAlias( const char *filename, bool is_lib );
diff --git a/bld/cc/c/cexpr.c b/bld/cc/c/cexpr.c
index 9cbb2cf..2333caf 100644
--- a/bld/cc/c/cexpr.c
+++ b/bld/cc/c/cexpr.c
@@ -50,26 +50,26 @@ struct mathfuncs {
     { NULL,      0, 0 }
 };
 
-local   TREEPTR GenNextParm(TREEPTR,TYPEPTR **);
-local   TREEPTR StartFunc(TREEPTR,TYPEPTR **);
-local   TREEPTR GetExpr(void);
-local   TREEPTR ExprId(void);
-local   TREEPTR ExprOpnd(void);
-local   TREEPTR SizeofOp(TYPEPTR);
-local   TREEPTR ScalarExpr(TREEPTR);
-local   TREEPTR UnaryPlus(TREEPTR);
-local   TREEPTR TernOp(TREEPTR,TREEPTR,TREEPTR);
-local   TREEPTR ColonOp(TREEPTR);
-local   TREEPTR StartTernary(TREEPTR);
-local   TREEPTR NotOp(TREEPTR);
-local   TREEPTR AndAnd(TREEPTR);
-local   TREEPTR OrOr(TREEPTR);
-local   TREEPTR GenFuncCall(TREEPTR);
-local   TREEPTR IndexOp(TREEPTR,TREEPTR);
-local   TREEPTR SegOp(TREEPTR,TREEPTR);
-local      void PopNestedParms( TYPEPTR **plistptr );
-local      void IncSymWeight( SYMPTR sym );
-local      void AddCallNode( TREEPTR tree );
+static   TREEPTR GenNextParm(TREEPTR,TYPEPTR **);
+static   TREEPTR StartFunc(TREEPTR,TYPEPTR **);
+static   TREEPTR GetExpr(void);
+static   TREEPTR ExprId(void);
+static   TREEPTR ExprOpnd(void);
+static   TREEPTR SizeofOp(TYPEPTR);
+static   TREEPTR ScalarExpr(TREEPTR);
+static   TREEPTR UnaryPlus(TREEPTR);
+static   TREEPTR TernOp(TREEPTR,TREEPTR,TREEPTR);
+static   TREEPTR ColonOp(TREEPTR);
+static   TREEPTR StartTernary(TREEPTR);
+static   TREEPTR NotOp(TREEPTR);
+static   TREEPTR AndAnd(TREEPTR);
+static   TREEPTR OrOr(TREEPTR);
+static   TREEPTR GenFuncCall(TREEPTR);
+static   TREEPTR IndexOp(TREEPTR,TREEPTR);
+static   TREEPTR SegOp(TREEPTR,TREEPTR);
+static      void PopNestedParms( TYPEPTR **plistptr );
+static      void IncSymWeight( SYMPTR sym );
+static      void AddCallNode( TREEPTR tree );
 
 extern    int64 LongValue64( TREEPTR leaf );
 
@@ -298,7 +298,7 @@ TREEPTR LongLeaf( target_long value )
     return( leaf );
 }
 
-local TREEPTR EnumLeaf( ENUMPTR ep )
+static TREEPTR EnumLeaf( ENUMPTR ep )
 {
     DATA_TYPE   decl_type;
     TREEPTR     leaf;
@@ -439,7 +439,7 @@ TREEPTR SymLeaf( void )
 }
 
 
-local void IncSymWeight( SYMPTR sym )
+static void IncSymWeight( SYMPTR sym )
 {
     static int  LoopWeights[] = { 1, 0x10, 0x100, 0x1000 };
 
@@ -499,7 +499,7 @@ static bool IsCallValue( TREEPTR tree )
 }
 
 // This RVALUE thing is backwards -mjc
-local TREEPTR TakeRValue( TREEPTR tree, int void_ok )
+static TREEPTR TakeRValue( TREEPTR tree, int void_ok )
 {
     TYPEPTR             typ;
     sym_flags           symb_flags;
@@ -953,7 +953,7 @@ FIELDPTR SearchFields( TYPEPTR *class_typ, target_size *field_offset, const char
 }
 
 
-local TYPEPTR Ptr2Struct( TYPEPTR typ )
+static TYPEPTR Ptr2Struct( TYPEPTR typ )
 {
     if( typ->decl_type != TYPE_POINTER ) {
         return( NULL );
@@ -1170,7 +1170,7 @@ TREEPTR SingleExpr( void )
 }
 
 
-local TREEPTR GetExpr( void )
+static TREEPTR GetExpr( void )
 {
     TREEPTR     tree, op1;
     TYPEPTR     typ;
@@ -1454,7 +1454,7 @@ local TREEPTR GetExpr( void )
 }
 
 
-local TREEPTR ExprOpnd( void )
+static TREEPTR ExprOpnd( void )
 {
     TREEPTR     tree;
     TYPEPTR     typ;
@@ -1617,7 +1617,7 @@ local TREEPTR ExprOpnd( void )
 }
 
 
-local bool IsMacroDefined( void )
+static bool IsMacroDefined( void )
 {
     MEPTR       mentry;
 
@@ -1634,7 +1634,7 @@ local bool IsMacroDefined( void )
 }
 
 
-local TREEPTR ExprId( void )
+static TREEPTR ExprId( void )
 {
     TREEPTR     tree;
     int         value;
@@ -1691,7 +1691,7 @@ local TREEPTR ExprId( void )
 
 
 #if 0
-local bool LValueArray( TREEPTR tree )
+static bool LValueArray( TREEPTR tree )
 {
     TYPEPTR     typ;
 
@@ -1710,7 +1710,7 @@ local bool LValueArray( TREEPTR tree )
 }
 #endif
 
-local TREEPTR GenIndex( TREEPTR tree, TREEPTR index_expr )
+static TREEPTR GenIndex( TREEPTR tree, TREEPTR index_expr )
 {
     TYPEPTR         typ;
     op_flags        tree_flags;
@@ -1774,14 +1774,14 @@ local TREEPTR GenIndex( TREEPTR tree, TREEPTR index_expr )
     return( tree );
 }
 
-local TREEPTR ArrayIndex( TREEPTR tree, TREEPTR index_expr )
+static TREEPTR ArrayIndex( TREEPTR tree, TREEPTR index_expr )
 {
     tree = GenIndex( tree, index_expr );
     return( tree );
 }
 
 
-local TREEPTR IndexOp( TREEPTR tree, TREEPTR index_expr )
+static TREEPTR IndexOp( TREEPTR tree, TREEPTR index_expr )
 {
     TYPEPTR     typ;
 
@@ -1810,7 +1810,7 @@ local TREEPTR IndexOp( TREEPTR tree, TREEPTR index_expr )
     return( tree );
 }
 
-local void AddCallNode( TREEPTR tree )
+static void AddCallNode( TREEPTR tree )
 // if a function call has no prototype wait till end
 // to check it out
 {
@@ -1852,7 +1852,7 @@ static int ParmNum( void )
     return( parm_count );
 }
 
-local TREEPTR GenNextParm( TREEPTR tree, TYPEPTR **plistptr )
+static TREEPTR GenNextParm( TREEPTR tree, TYPEPTR **plistptr )
 {
     TYPEPTR     *plist;
     TYPEPTR     typ;
@@ -1923,7 +1923,7 @@ local TREEPTR GenNextParm( TREEPTR tree, TYPEPTR **plistptr )
 }
 
 
-local bool IntrinsicMathFunc( SYM_NAMEPTR sym_name, int i, size_t len, SYMPTR sym )
+static bool IntrinsicMathFunc( SYM_NAMEPTR sym_name, int i, size_t len, SYMPTR sym )
 {
     size_t  j;
 
@@ -1946,7 +1946,7 @@ local bool IntrinsicMathFunc( SYM_NAMEPTR sym_name, int i, size_t len, SYMPTR sy
 #else
     #define REG_SIZE    4
 #endif
-local TREEPTR GenVaStartNode( TREEPTR last_parm )
+static TREEPTR GenVaStartNode( TREEPTR last_parm )
 {
     // there should be 3 parms __builtin_va_start( list, parm_name, stdarg )
     // - first parm should be name of va_list
@@ -1999,7 +1999,7 @@ local TREEPTR GenVaStartNode( TREEPTR last_parm )
     return( tree );
 }
 
-local TREEPTR GenAllocaNode( TREEPTR size_parm )
+static TREEPTR GenAllocaNode( TREEPTR size_parm )
 {
     // there should be 1 parm __builtin_alloca( size )
     TREEPTR     tree;
@@ -2016,7 +2016,7 @@ local TREEPTR GenAllocaNode( TREEPTR size_parm )
 #endif
 
 #if _CPU == _PPC
-local TREEPTR GenVaArgNode( TREEPTR last_parm )
+static TREEPTR GenVaArgNode( TREEPTR last_parm )
 {
     // there should be 2 parms __builtin_varg( list, type_arg )
     // - first parm should be name of va_list
@@ -2049,7 +2049,7 @@ local TREEPTR GenVaArgNode( TREEPTR last_parm )
 }
 #endif
 
-local TREEPTR GenFuncCall( TREEPTR last_parm )
+static TREEPTR GenFuncCall( TREEPTR last_parm )
 {
     expr_level_type i;
     expr_level_type n;
@@ -2201,7 +2201,7 @@ done_call:
     return( tree );
 }
 
-local void PopNestedParms( TYPEPTR **plistptr )
+static void PopNestedParms( TYPEPTR **plistptr )
 {
     struct nested_parm_lists    *npl;
 
@@ -2231,7 +2231,7 @@ static TREEPTR DummyFuncName( void )
     return( tree );
 }
 
-local TREEPTR StartFunc( TREEPTR tree, TYPEPTR **plistptr )
+static TREEPTR StartFunc( TREEPTR tree, TYPEPTR **plistptr )
 {
     TYPEPTR             typ;
     TYPEPTR             orig_typ;
@@ -2378,7 +2378,7 @@ local TREEPTR StartFunc( TREEPTR tree, TYPEPTR **plistptr )
 }
 
 
-local TREEPTR OrOr( TREEPTR tree )
+static TREEPTR OrOr( TREEPTR tree )
 {
     // This routine is called when || token is found
     if( tree->op.opr == OPR_PUSHINT ) {
@@ -2391,7 +2391,7 @@ local TREEPTR OrOr( TREEPTR tree )
 }
 
 
-local TREEPTR AndAnd( TREEPTR tree )
+static TREEPTR AndAnd( TREEPTR tree )
 {
     // This routine is called when && token is found
     if( tree->op.opr == OPR_PUSHINT ) {
@@ -2439,7 +2439,7 @@ TREEPTR BoolExpr( TREEPTR tree )
 }
 
 
-local TREEPTR NotOp( TREEPTR tree )
+static TREEPTR NotOp( TREEPTR tree )
 {
     TREEPTR     opnd;
     FLOATVAL    *flt;
@@ -2492,14 +2492,14 @@ local TREEPTR NotOp( TREEPTR tree )
 }
 
 
-local TREEPTR StartTernary( TREEPTR tree )
+static TREEPTR StartTernary( TREEPTR tree )
 {
     tree = BoolExpr( tree );
     return( tree );
 }
 
 
-local TREEPTR ColonOp( TREEPTR true_part )
+static TREEPTR ColonOp( TREEPTR true_part )
 {
     if( Class[ExprLevel] != TC_TERNARY ) {
         CErr1( ERR_MISSING_QUESTION_OR_MISPLACED_COLON );
@@ -2574,7 +2574,7 @@ static TREEPTR TernOp( TREEPTR expr1, TREEPTR true_part, TREEPTR false_part )
 }
 
 
-local TREEPTR UnaryPlus( TREEPTR tree )
+static TREEPTR UnaryPlus( TREEPTR tree )
 {
     TYPEPTR     typ;
 
@@ -2627,7 +2627,7 @@ TREEPTR ScalarExpr( TREEPTR tree )
 }
 
 
-local TREEPTR SizeofOp( TYPEPTR typ )
+static TREEPTR SizeofOp( TYPEPTR typ )
 {
     TREEPTR             tree;
     target_size         size;
@@ -2658,7 +2658,7 @@ local TREEPTR SizeofOp( TYPEPTR typ )
     return( tree );
 }
 
-local TREEPTR SegOp( TREEPTR seg, TREEPTR offset )
+static TREEPTR SegOp( TREEPTR seg, TREEPTR offset )
 {
     TREEPTR     tree;
     TYPEPTR     typ;
diff --git a/bld/cc/c/ctype.c b/bld/cc/c/ctype.c
index 12d8062..7561b22 100644
--- a/bld/cc/c/ctype.c
+++ b/bld/cc/c/ctype.c
@@ -33,10 +33,10 @@
 
 extern  unsigned SymTypedef;
 
-local TYPEPTR StructDecl(DATA_TYPE,bool);
-//local TYPEPTR ComplexDecl(int,bool);
-local void      SetPlainCharType( DATA_TYPE char_type );
-local void CheckBitfieldType( TYPEPTR typ );
+static TYPEPTR StructDecl(DATA_TYPE,bool);
+//static TYPEPTR ComplexDecl(int,bool);
+static void      SetPlainCharType( DATA_TYPE char_type );
+static void CheckBitfieldType( TYPEPTR typ );
 
 #if _CPU == 386
 #define _CHECK_SIZE( s )
@@ -325,7 +325,7 @@ type_modifiers TypeQualifier( void )
     return( flags );
 }
 
-local TYPEPTR GetScalarType( bool *plain_int, int bmask, type_modifiers flags )
+static TYPEPTR GetScalarType( bool *plain_int, int bmask, type_modifiers flags )
 {
     DATA_TYPE   data_type;
     TYPEPTR     typ;
@@ -400,7 +400,7 @@ local TYPEPTR GetScalarType( bool *plain_int, int bmask, type_modifiers flags )
 }
 
 
-local void AdvanceToken( void )
+static void AdvanceToken( void )
 {
     if( CurToken == T_SAVED_ID ) {
         CMemFree( SavedId );
@@ -763,7 +763,7 @@ TAGPTR VfyNewTag( TAGPTR tag, DATA_TYPE tag_type )
 }
 
 
-local FIELDPTR NewField( FIELDPTR new_field, TYPEPTR decl )
+static FIELDPTR NewField( FIELDPTR new_field, TYPEPTR decl )
 {
     FIELDPTR    field;
     FIELDPTR    prev_field;
@@ -823,7 +823,7 @@ local FIELDPTR NewField( FIELDPTR new_field, TYPEPTR decl )
 }
 
 
-local TYPEPTR EnumFieldType( TYPEPTR ftyp, bool plain_int,
+static TYPEPTR EnumFieldType( TYPEPTR ftyp, bool plain_int,
                     bitfield_width start, bitfield_width width )
 {
     TYPEPTR     typ;
@@ -877,7 +877,7 @@ align_type GetTypeAlignment( TYPEPTR typ )
 }
 
 
-local target_size FieldAlign( target_size next_offset, FIELDPTR field, align_type *worst_alignment )
+static target_size FieldAlign( target_size next_offset, FIELDPTR field, align_type *worst_alignment )
 {
     align_type  pack_adjustment;
     align_type  align;
@@ -902,7 +902,7 @@ local target_size FieldAlign( target_size next_offset, FIELDPTR field, align_typ
     return( next_offset );
 }
 
-local DATA_TYPE UnQualifiedType( TYPEPTR typ )
+static DATA_TYPE UnQualifiedType( TYPEPTR typ )
 {
     SKIP_TYPEDEFS( typ );
     SKIP_ENUM( typ );
@@ -932,7 +932,7 @@ local DATA_TYPE UnQualifiedType( TYPEPTR typ )
 
 /* clear the hash table of all fields that were just defined
    in the struct with tag tag */
-local void ClearFieldHashTable( TAGPTR tag )
+static void ClearFieldHashTable( TAGPTR tag )
 {
     FIELDPTR field;
     FIELDPTR hash_field;
@@ -984,7 +984,7 @@ static void AdjFieldTypeNode( FIELDPTR field, type_modifiers decl_mod )
     }
 }
 
-local target_size GetFields( TYPEPTR decl )
+static target_size GetFields( TYPEPTR decl )
 {
     target_size         start;
     TYPEPTR             typ;
@@ -1129,7 +1129,7 @@ local target_size GetFields( TYPEPTR decl )
 }
 
 
-local TYPEPTR StructDecl( DATA_TYPE decl_typ, bool packed )
+static TYPEPTR StructDecl( DATA_TYPE decl_typ, bool packed )
 {
     TYPEPTR     typ;
     TAGPTR      tag;
@@ -1202,7 +1202,7 @@ struct {
 */
 
 /*
-local void GetComplexFieldTypeSpecifier( decl_info *info, DATA_TYPE data_type )
+static void GetComplexFieldTypeSpecifier( decl_info *info, DATA_TYPE data_type )
 {
     info->stg = SC_NONE;      // indicate don't want any storage class specifiers
     info->mod = FLAG_NONE;
@@ -1214,7 +1214,7 @@ local void GetComplexFieldTypeSpecifier( decl_info *info, DATA_TYPE data_type )
 }
 
 
-local target_size GetComplexFields( TYPEPTR decl )
+static target_size GetComplexFields( TYPEPTR decl )
 {
     target_size         start;
     TYPEPTR             typ;
@@ -1270,7 +1270,7 @@ local target_size GetComplexFields( TYPEPTR decl )
 }
 
 
-local TYPEPTR ComplexDecl( DATA_TYPE decl_typ, bool packed )
+static TYPEPTR ComplexDecl( DATA_TYPE decl_typ, bool packed )
 {
     TYPEPTR     typ;
     TAGPTR      tag;
@@ -1297,7 +1297,7 @@ local TYPEPTR ComplexDecl( DATA_TYPE decl_typ, bool packed )
 }
 */
 
-local void CheckBitfieldType( TYPEPTR typ )
+static void CheckBitfieldType( TYPEPTR typ )
 {
     SKIP_TYPEDEFS( typ );
     if( CompFlags.extensions_enabled ) {
@@ -1437,7 +1437,7 @@ TYPEPTR ArrayNode( TYPEPTR the_object )
 }
 
 
-local TYPEPTR MkPtrNode( TYPEPTR typ, type_modifiers flags,
+static TYPEPTR MkPtrNode( TYPEPTR typ, type_modifiers flags,
     segment_id segid, SYM_HANDLE base, BASED_KIND based_kind )
 {
     TYPEPTR     ptrtyp;
-- 
1.8.3.4

